namespace RMS.Track.Tests

open Microsoft.VisualStudio.TestTools.UnitTesting
open Newtonsoft.Json.Linq
open System.Collections.Generic
open RMS.Track.Processor
open System
open System.Data
open TestData

[<TestClass>]
type TestClass () =
    let functions1 : (SendGridEvent -> bool)[] = 
        [|
            fun evt -> evt.EmailId = -1L
            fun evt -> evt.TimeStamp = 0
            fun evt -> evt.EmailId = -1L
        |]

    let functions2 : (SendGridEvent -> bool)[] = 
        [|
            fun evt -> evt.Email = "?"
            fun evt -> evt.EmailId = -1L
            fun evt -> evt.TimeStamp = 0
            fun evt -> evt.EventType = "unspecified"
            fun evt -> evt.Url = null
        |]

    [<DataTestMethod>]
    [<DataRow("example@tes.com", "334346263473835685684568358", "4", "processed", "info@renewtrak.com", 0) >]
    [<DataRow("example@tes.com", "34", "23525261636252534", "processed", "info@renewtrak.com", 1)>]
    [<DataRow("example@tes.com", null, "4", "processed", "info@renewtrak.com", 2)>]
    member _.``Can detect json incorrect fields``(email:string, emailid:string, timestamp:string, event:string, url:string, fIndex:int) =
        Assert.IsTrue(SendGridEvent(JObject.Parse(jsonTemplate email emailid timestamp event url)) |> functions1.[fIndex])

    [<DataTestMethod>]
    [<DataRow("example@tes.com", 2, 3, "processed", "info@renewtrak.com")>]
    member _.``Can convert correct json``(email:string, emailid:int64, timestamp:int, event:string, url:string) =
        let sge = SendGridEvent(JObject.Parse(jsonTemplate email emailid timestamp event url))
        Assert.AreEqual(email, sge.Email)
        Assert.AreEqual(emailid, sge.EmailId)
        Assert.AreEqual(timestamp, sge.TimeStamp)
        Assert.AreEqual(event, sge.EventType)
        Assert.AreEqual(url, sge.Url)

    [<DataTestMethod>]
    [<DataRow("email", 0)>]
    [<DataRow("emailid", 1)>]
    [<DataRow("timestamp", 2)>]
    [<DataRow("event", 3)>]
    [<DataRow("url", 4)>]
    member _.``Can detect missing json field``(fieldToRemove:string, fIndex:int) =
        let jo = JObject.Parse(validJson)
        jo.Remove(fieldToRemove) |> ignore
        let sge = SendGridEvent(jo)
        Assert.IsTrue(functions2.[fIndex] sge)

    static member private GetTables() = tables()

    [<TestMethod>]
    [<DynamicData(nameof TestClass.GetTables, DynamicDataSourceType.Method)>]
    member _.``Can validate datatable``(table:DataTable, expectedValid:bool, message:string) =
        if expectedValid then
            try DataExtensions.Validated(table) |> ignore
            with | _ -> Assert.Fail(message)
        else
            try 
                DataExtensions.Validated(table) |> ignore
                Assert.Fail(message)
            with | :? ApplicationException -> ()

