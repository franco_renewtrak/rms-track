﻿module TestData

open System
open System.Data

let jsonTemplate email emailid timestamp event url = 
    $"""
    {{
        "email": "{email}",
        "emailid": {emailid},
        "timestamp": {timestamp},
        "smtp-id": "<14c5d75ce93.dfd.64b469@ismtpd-555>",
        "event": "{event}",
        "category": [
        "cat facts"
        ],
        "url": "{url}",
        "sg_event_id": "g7GagN_yD1ITs2BGSf6-bg==",
        "sg_message_id": "14c5d75ce93.dfd.64b469.filter0001.16648.5515E0B88.0"
    }}
    """

let validJson = jsonTemplate "example@tes.com" "6" "4" "processed" "info@renewtrak.com"

let table columns = 
    let table = new DataTable()
    columns |> Seq.iter (fun (name, typ) -> table.Columns.Add(name, typ) |> ignore)
    table

let validTable = 
    table [
        "EmailMessageID", typeof<int64>
        "EventType", typeof<string>
        "Email", typeof<string>
        "EventTS", typeof<DateTime>
        "Url", typeof<string>
        "CreatedID", typeof<int>
        "CreatedSessionID", typeof<int64>
        "CreatedTS", typeof<DateTime> ]

let tables() = 
    let testData (columns, expected, message) = [| table columns |> box; box expected; box message |]

    seq {
       [
        "EmailMessageID", typeof<int64>
        "EventType", typeof<string>
        "Email", typeof<string>
        "EventTS", typeof<DateTime>
        "Url", typeof<string>
        "CreatedID", typeof<int>
        "CreatedSessionID", typeof<int64>
        "CreatedTS", typeof<DateTime>
       ]
       , true, "1. should be validated"

       [
        "EmailMessageID", typeof<int64>
        "EventType", typeof<string>
        "Email", typeof<string>
        "23tc245yv2", typeof<DateTime>
        "Url", typeof<string>
        "CreatedID", typeof<int>
        "CreatedSessionID", typeof<int64>
        "CreatedTS", typeof<DateTime>
       ]
       , false, "3. incorrect field name"

       [
        "EmailMessageId", typeof<int>
        "EventType", typeof<string>
        "Email", typeof<string>
        "EventTS", typeof<DateTime>
        "Url", typeof<string>
        "CreatedID", typeof<int>
        "CreatedSessionID", typeof<int64>
        "CreatedTS", typeof<DateTime>
       ]
       , false, "4. incorrect field type"

       [
        "EmailMessageId", typeof<int>
        "EventType", typeof<string>
        "Email", typeof<string>
        "EventTS", typeof<DateTime>
        "CreatedID", typeof<int>
        "CreatedSessionID", typeof<int64>
        "CreatedTS", typeof<DateTime>
       ]
       , false, "5. missing field"

       [
        "JohnDoe", typeof<string>
        "EmailMessageID", typeof<int64>
        "EventType", typeof<string>
        "Email", typeof<string>
        "EventTS", typeof<DateTime>
        "Url", typeof<string>
        "CreatedID", typeof<int>
        "CreatedSessionID", typeof<int64>
        "CreatedTS", typeof<DateTime>
       ]
       , true, "6. additional field"

       [
        "EMAILMESSAGEID", typeof<int64>
        "eventtype", typeof<string>
        "EmaIl", typeof<string>
        "EvENtTS", typeof<DateTime>
        "url", typeof<string>
        "CreatedID", typeof<int>
        "CreatedSessionID", typeof<int64>
        "CreatedTS", typeof<DateTime>
       ]
       , true, "7. case insentitive"

    } |> Seq.map testData




