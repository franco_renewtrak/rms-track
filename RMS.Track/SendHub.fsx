#r "nuget: Microsoft.Azure.EventHubs"

open Microsoft.Azure.EventHubs
open System.Text

let EventHubConnectionString = "Endpoint=sb://emailactivity.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=4jhbTJz8vhvCVcxiqiZV4GVcOWAEq7Ytg664zSOwQ7U="
let EventHubName = "sendgridwebhook"

let connectionStringBuilder = new EventHubsConnectionStringBuilder(EventHubConnectionString, EntityPath = EventHubName)

let eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString())

let json = 
            """
            [
            	{
            		"email": "example1@test.com",
            		"emailid": 1,
            		"timestamp": 1,
            		"smtp-id": "<14c5d75ce93.dfd.64b469@ismtpd-555>",
            		"event": "click",
            		"url": "http://aaa.bbb.ccc",
            		"category": [
            			"cat facts"
            		],
            		"sg_event_id": "g7GagN_yD1ITs2BGSf6-bg==",
            		"sg_message_id": "14c5d75ce93.dfd.64b469.filter0001.16648.5515E0B88.0"
            	},
            	{
            		"email": "example2@test.com",
            		"emailid": 2,
            		"timestamp": 2,
            		"smtp-id": "<14c5d75ce93.dfd.64b469@ismtpd-555>",
            		"event": "deferred",
            		"category": [
            			"cat facts"
            		],
            		"sg_event_id": "SN5wtB5nlQWgf2v3ImyvHA==",
            		"sg_message_id": "14c5d75ce93.dfd.64b469.filter0001.16648.5515E0B88.0",
            		"response": "400 try again later",
            		"attempt": "5"
            	}
            ]
            """

let header = [| 6uy; 0uy; 0uy; 0uy; byte('l'); byte('e'); byte('n'); byte('o'); byte('v'); byte('o') |]
do eventHubClient.SendAsync(new EventData([header; Encoding.UTF8.GetBytes(json)] |> Array.concat)) |> Async.AwaitTask |> Async.RunSynchronously