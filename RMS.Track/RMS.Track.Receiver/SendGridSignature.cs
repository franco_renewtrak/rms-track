﻿using EllipticCurve;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace RMS.Track.Receiver
{
    /// <summary>
    /// This class allows you to use the Event Webhook feature. Read the docs for
    /// more details: https://sendgrid.com/docs/for-developers/tracking-events/event
    /// </summary>
    static class SendGridSignature
    {
        /// <summary>
        /// Signature verification HTTP header name for the signature being sent.
        /// </summary>
        public const string SIGNATURE_HEADER = "X-Twilio-Email-Event-Webhook-Signature";

        /// <summary>
        /// Timestamp HTTP header name for timestamp.
        /// </summary>
        public const string TIMESTAMP_HEADER = "X-Twilio-Email-Event-Webhook-Timestamp";

        /// <summary>
        /// Convert the public key string to a <see cref="PublicKey"/>.
        /// </summary>
        /// <param name="publicKey">verification key under Mail Settings</param>
        /// <returns>public key using the ECDSA algorithm</returns>
        public static PublicKey ConvertPublicKeyToECDSA(string publicKey)
        {
            return PublicKey.fromPem(publicKey);
        }

        /// <summary>
        /// Verify signed event webhook requests.
        /// </summary>
        /// <param name="publicKey">elliptic curve public key</param>
        /// <param name="payload">event payload in the request body</param>
        /// <param name="signature">value obtained from the 'X-Twilio-Email-Event-Webhook-Signature' header</param>
        /// <param name="timestamp">value obtained from the 'X-Twilio-Email-Event-Webhook-Timestamp' header</param>
        /// <returns>true or false if signature is valid</returns>
        public static bool VerifySignature(PublicKey publicKey, string payload, string signature, string timestamp)
        {
            var timestampedPayload = timestamp + payload;
            var decodedSignature = Signature.fromBase64(signature);

            return Ecdsa.verify(timestampedPayload, decodedSignature, publicKey);
        }

        public static bool Authorize(string? key, HttpRequest req, ReadOnlySpan<byte> bytes)
        {
            if (string.IsNullOrEmpty(key)) throw new ArgumentException($"Missing public key");
            var signature = req.Headers[SIGNATURE_HEADER];
            if (string.IsNullOrEmpty(signature)) throw new ArgumentException($"Missing signature [{SIGNATURE_HEADER}]");
            var timestamp = req.Headers[TIMESTAMP_HEADER];
            if (string.IsNullOrEmpty(timestamp)) throw new ArgumentException($"Missing timestamp [{TIMESTAMP_HEADER}]");
            var publicKey = PublicKey.fromPem(key);
            return Encoding.UTF8.GetString(bytes) is string payload && VerifySignature(publicKey, payload, signature, timestamp);
        }
    }
}
