using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using RMS.Track.Common;
using System;
using System.Buffers.Binary;
using System.Text;
using System.Threading.Tasks;

namespace RMS.Track.Receiver
{
    public static class RmsTrackReceiver
    {
        /*
            https://docs.sendgrid.com/for-developers/tracking-events/getting-started-event-webhook
            Events post every 30 seconds or when the batch size reaches 768 kilobytes - whichever occurs first. 
            This is per server, so if you send a high volume of emails, the webhook URL may receive tens or even hundreds of posts per second.
        */
        const int KILOBYTE = 1024;
        const int MEGABYTE = 1024 * KILOBYTE;
        const int MAX_SENDGRID_BATCH_SIZE = 768 * KILOBYTE;
        const int MAX_HUB_MESSAGE_SIZE_PREMIUM = MEGABYTE;
        const int HEADER_SIZE = sizeof(int);
        const int MAX_TENANT_SIZE = MAX_HUB_MESSAGE_SIZE_PREMIUM - HEADER_SIZE - MAX_SENDGRID_BATCH_SIZE;
                                   
        [FunctionName("RmsTrackReceiver")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "analytics/email/{tenant}")] HttpRequest req, string tenant
            , [EventHub("sendgridwebhook", Connection = "EventHubConnectionString")] IAsyncCollector<byte[]> outputEvents
            , ILogger log
            )
        {
            try
            {
                byte[] tenantBytes;
                int tenantSize;
                try
                {
                    if (string.IsNullOrWhiteSpace(tenant)) throw new InvalidOperationException($"Tenant null or whitespace ['{tenant}']");
                    tenantBytes = Encoding.ASCII.GetBytes(tenant);
                    tenantSize = tenantBytes.Length;
                    if (tenantSize > MAX_TENANT_SIZE) throw new InvalidOperationException($"Tenant too long [{tenant.Length}]");
                }
                catch (Exception ex)
                {
                    log.LogError($"Invalid Tenant [{ex.Message}]");
                    return new BadRequestResult();
                }

                int bodySize = (int)req.Body.Length;
                if (bodySize > MAX_SENDGRID_BATCH_SIZE)
                {
                    log.LogError("Message size {bodyLength} exceeds declared maximum", bodySize);
                    return new BadRequestResult();
                }

                if (bodySize == 0)
                {
                    log.LogError("Message empty");
                    return new NoContentResult();
                }

                int bufferSize = HEADER_SIZE + tenantSize + bodySize;
                byte[] buffer = new byte[bufferSize];
                BinaryPrimitives.WriteInt32LittleEndian(new Span<byte>(buffer, 0, HEADER_SIZE), tenantSize);
                tenantBytes.CopyTo(buffer, HEADER_SIZE);
                await req.Body.ReadAsync(buffer, HEADER_SIZE + tenantSize, bodySize);

                try
                {
                    if (!SendGridSignature.Authorize(await KeyVault.GetSendGridPublicKeyAsync(tenant), req, new ReadOnlySpan<byte>(buffer, HEADER_SIZE + tenantSize, bodySize)))
                    {
                        log.LogError($"Authorization denied.");
                        return new UnauthorizedResult();
                    }
                }
                catch(Exception ex)
                { 
                    log.LogError($"Authorization error [{ex}].");
                    return new UnauthorizedResult();
                }

                await outputEvents.AddAsync(buffer);
            }
            catch(Exception ex)
            {
                log.LogError($"Exception triggered [{ex}]");
                return new BadRequestResult();
            }
            return new OkResult();
        }
    }
}
