﻿using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RMS.Track.Common
{
    public static class KeyVault
    {
        static string _vaultUri = Environment.GetEnvironmentVariable("VaultUri");
        static ConcurrentDictionary<string, string> _cache = new ConcurrentDictionary<string, string>();

        public static async Task<string?> GetSendGridPublicKeyAsync(string tenant)
        {
            var vaultKey = $@"{_vaultUri}secrets/{tenant}-sendgrid-webhook-publickey";
            return await GetAsync(vaultKey);
        }

        public static async Task<string?> GetConnectionStringAsync(string tenant)
        {
            var vaultKey = $@"{_vaultUri}secrets/{tenant}-connection-string";
            return await GetAsync(vaultKey);
        }

        static async Task<string?> GetAsync(string key)
        {
            if (_cache.TryGetValue(key, out var cached)) return cached;
            if (await FetchAsync(key) is string value) return _cache[key] = value;
            return null;

            async Task<string?> FetchAsync(string key)
            {
                AzureServiceTokenProvider azureServiceTokenProvider = new AzureServiceTokenProvider();
                var keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));
                var secretBundle = await keyVaultClient.GetSecretAsync(key).ConfigureAwait(false);
                return secretBundle?.Value;
            }
        }
    }
}
