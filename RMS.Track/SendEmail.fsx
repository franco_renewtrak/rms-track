#r "nuget: SendGrid"

open SendGrid
open SendGrid.Helpers.Mail

[<Literal>]
let sgTenantDevKey = "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"
[<Literal>]
let sgTenantTestsKey = "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"

type Tenant = {
    Email: string
    SgApiKey: string
}

type Tenants = | Dev | Tests

let tenants = 
    [
        Dev,  {Email = "renewtrakdev@renewtrak.com"; SgApiKey = sgTenantDevKey}
        Tests,  {Email = "renewtraktests@renewtrak.com"; SgApiKey = sgTenantTestsKey}
    ] |> Map.ofList

let send (to':string) messages = 
    let clients = [| SendGridClient(tenants.[Dev].SgApiKey); SendGridClient(tenants.[Tests].SgApiKey) |]
    let tenants = [| tenants.[Dev]; tenants.[Tests] |]
    
    let email i (subject, content) = 
        let message = SendGridMessage()
        message.AddTo(to')
        message.SetFrom(tenants.[i % 2].Email)
        message.Subject <- subject
        message.AddContent("test/plain", content)
        message.AddCustomArg("emailID", string i);

        clients.[i % 2].SendEmailAsync(message) |> Async.AwaitTask

    let sendChunk i chunk = 
        printf "Start Chunk %d -> " i
        let resps = chunk |> Seq.mapi email |> Async.Parallel |> Async.RunSynchronously
        printfn "%A" (resps |> Seq.forall (fun resp -> resp.IsSuccessStatusCode))

    messages 
    |> Seq.chunkBySize 1000 
    |> Seq.mapi (fun i chunk -> (i, chunk))
    |> Seq.iter (fun (i, chunk) -> sendChunk i chunk)


let messages n = [for i in 1..n -> ($"subject{i}", $"content{i}")]

//messages 10 |> send "dev@pluto.com" tenants.[Dev]

messages 3500 |> send "tests@pluto.com"
messages 1 |> send "vikrant.sethi@renewtrak.com"

//let client = SendGridClient("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")

//let rsp = client.SendEmailAsync(message) |> Async.AwaitTask |> Async.RunSynchronously
//rsp

//[
//    for i = 1 to 1000 do 
//        async { 
//            printfn "%d" i
//            do! client.SendEmailAsync(message) |> Async.AwaitTask |> Async.Ignore
//        }
//] |> Async.Parallel |> Async.RunSynchronously
