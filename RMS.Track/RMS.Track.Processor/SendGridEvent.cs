﻿using Newtonsoft.Json.Linq;

namespace RMS.Track.Processor
{
    internal class SendGridEvent
    {
        public string Email { get; private set; }
        public long EmailId { get; private set; }
        public string EventType { get; private set; }
        public int TimeStamp { get; private set; }
        public string? Url { get; private set; }

        public SendGridEvent(JObject sendGridEvent)
        {
            Email = GetOrDefault("email", "?");
            EmailId = GetOrDefault("emailid", -1L);
            TimeStamp = GetOrDefault("timestamp", 0);
            EventType = GetOrDefault("event", "unspecified");
            Url = GetOrDefault<string?>("url", null);

            T GetOrDefault<T>(string name, T defaultValue)
            {
                try { return sendGridEvent.GetValue(name, System.StringComparison.OrdinalIgnoreCase).ToObject<T>(); }
                catch { return defaultValue; }
            }
        }

        public override string ToString() => $"SendGridEvent(Email = {Email}, EmailId = {EmailId}, EventType = {EventType}, TimeStamp = {TimeStamp}, Url = {Url})";
    }
}

#region definitions
/*
 * https://docs.sendgrid.com/for-developers/tracking-events/event#json-objects
# Default SendGrid message json fields:

email - the email address of the recipient
timestamp - the UNIX timestamp of when the message was sent
event - the event type. Possible values are processed, dropped, delivered, deferred, bounce, open, click, spam report, unsubscribe, group unsubscribe, and group resubscribe.
smtp-id - a unique ID attached to the message by the originating system.
useragent - the user agent responsible for the event. This is usually a web browser. For example, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36".
ip - the IP address used to send the email. For open and click events, it is the IP address of the recipient who engaged with the email.
sg_event_id - a unique ID to this event that you can use for deduplication purposes. These IDs are up to 100 characters long and are URL safe.
sg_message_id - a unique, internal SendGrid ID for the message. The first half of this ID is pulled from the smtp-id. The message ID will be included in most cases. In the event of an asynchronous bounce, the message ID will not be available. An asynchronous bounce occurs when a message is first accepted by the receiving mail server and then bounced at a later time. When this happens, there is less information available about the bounce.
reason - any sort of error response returned by the receiving server that describes the reason this event type was triggered.
status - status code string. Corresponds to HTTP status code - for example, a JSON response of 5.0.0 is the same as a 500 error response.
response - the full text of the HTTP response error returned from the receiving server.
tls - indicates whether TLS encryption was used in sending this message. For more information about TLS, see the TLS Glossary page.
url - the URL where the event originates. For click events, this is the URL clicked on by the recipient.
url_offset - if there is more than one of the same links in an email, this tells you which of those identical links was clicked.
attempt - the number of times SendGrid has attempted to deliver this message.
category - Categories are custom tags that you set for the purpose of organizing your emails. If you send single categories as an array, they will be returned by the webhook as an array. If you send single categories as a string, they will be returned by the webhook as a string.
type - indicates whether the bounce event was a hard bounce (type=bounce) or block (type=blocked)


 * https://docs.sendgrid.com/for-developers/tracking-events/event#event-objects

                          ProcessedDropped DelivereDeferredBounceOpened  ClickeSpam ReportUnsubscribeGroup UnsubscribeGroup Resubscribe
email                     X        X       X       X       X     X       X     X          X          X                X
timestamp                 X        X       X       X       X     X       X     X          X          X                X
event                     X        X       X       X       X     X       X     X          X          X                X
smtp-id                   X        X       X       X       X
useragent                                                        X       X                           X                X
ip                                         X       X       X     X       X                           X                X
sg_event_id               X        X       X       X       X     X       X     X          X          X                X
sg_message_id             X        X       X       X       X     X       X     X          X          X                X
reason                             X               X       X
status                                                     X
response                                   X
tls                                        X               X
url                                                                      X
category                  X        X       X       X       X     X       X     X          X
asm_group_id              X        X       X       X       X     X       X                           X                X
unique_args               X        X       X       X       X     X       X     X          X          X                X
marketing_campaign_id     X        X       X       X       X     X       X     X          X          X                X
marketing_campaign_name   X        X       X       X       X     X       X     X          X          X                X
attempt                                            X
pool                      X


 * Renewtrak format with custom fields
  
 	{
		"email": "example@test.com",
		"timestamp": 1623932636,
		"smtp-id": "<14c5d75ce93.dfd.64b469@ismtpd-555>",
		"event": "processed",
        "emailid": 123,*
		"category": [
			"cat facts"
		],
		"sg_event_id": "g7GagN_yD1ITs2BGSf6-bg==",
		"sg_message_id": "14c5d75ce93.dfd.64b469.filter0001.16648.5515E0B88.0",
        "response": "string",*
        "attempt": "string",*
        "useragent": "string",*
        "ip": "string",*
        "url": "string",*
        "reason": "string",*
        "status": "string",*
        "asm_group_id": "integer"*
	}


*/
#endregion