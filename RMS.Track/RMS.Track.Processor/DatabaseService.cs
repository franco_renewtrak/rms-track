﻿using Microsoft.Azure.EventHubs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System;
using System.Threading.Tasks;
using RMS.Track.Common;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Buffers.Binary;

namespace RMS.Track.Processor
{
    public class DatabaseService : IDatabaseService
    {
        const int KILOBYTE = 1024;
        const int MEGABYTE = 1024 * KILOBYTE;
        const int MAX_SENDGRID_BATCH_SIZE = 768 * KILOBYTE;
        const int MAX_HUB_MESSAGE_SIZE_PREMIUM = MEGABYTE;
        const int HEADER_SIZE = sizeof(int);
        const int MAX_TENANT_SIZE = MAX_HUB_MESSAGE_SIZE_PREMIUM - HEADER_SIZE - MAX_SENDGRID_BATCH_SIZE;
        const string RMS_TABLE_NAME = "EmailEvent";
        const string SQL_SCHEMA_GET = "SELECT TOP 0 * FROM " + RMS_TABLE_NAME;
        const string BULK_WRITE_TIMEOUT_CONFIG = "SqlBulkTimeout";
        readonly int _bulkWriteTimeout = 30;

        public DatabaseService()
        {
            try
            {
                _bulkWriteTimeout = int.Parse(Environment.GetEnvironmentVariable(BULK_WRITE_TIMEOUT_CONFIG)!);
            }
            catch { }
        }

        public async Task ProcessEvent(EventData eventData)
        {
            var (tenant, sendGridEvents) = Extract(eventData);
            var (table, connectionString) = await GetDataTable(tenant);

            var rows = Enumerable.Range(0, sendGridEvents.Length).Select(_ => table.NewRow()).ToArray();
            Parallel.For(0, sendGridEvents.Length, i => rows[i].FillWith(sendGridEvents[i]));

            foreach (var row in rows) table.Rows.Add(row);
            await CommitAsync(table, connectionString, _bulkWriteTimeout);

            #region Helpers
            static (string tenant, SendGridEvent[]) Extract(EventData eventData)
            {
                if (eventData.Body.Array == null) throw new ApplicationException("Empty message received");
                var body = eventData.Body;
                byte[] buffer = body.Array;

                string tenant;
                int tenantSize = BinaryPrimitives.ReadInt32LittleEndian(new ReadOnlySpan<byte>(buffer, 0, HEADER_SIZE));
                if (tenantSize < 1 || tenantSize > MAX_TENANT_SIZE) throw new ApplicationException("Tenant too long");
                tenant = Encoding.ASCII.GetString(buffer, HEADER_SIZE, tenantSize);

                TextReader reader = new StreamReader(new MemoryStream(buffer, body.Offset + HEADER_SIZE + tenantSize, body.Count - HEADER_SIZE - tenantSize));
                var jsonReader = new JsonTextReader(reader);
                var sendGridEvents = JArray.Load(jsonReader).Select(eventJToken => new SendGridEvent((JObject)eventJToken)).ToArray();
                return (tenant, sendGridEvents);
            }

            static async Task CommitAsync(DataTable table, string connectionString, int timeout)
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var bulk = new SqlBulkCopy(connection))
                    {
                        bulk.BulkCopyTimeout = timeout;
                        bulk.DestinationTableName = table.TableName;
                        await bulk.WriteToServerAsync(table);
                    }
                }
            }

            static async Task<(DataTable, string)> GetDataTable(string tenant)
            {
                if (await KeyVault.GetConnectionStringAsync(tenant) is string connectionString)
                {
                    DataTable table = new(RMS_TABLE_NAME);
                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (var adapter = new SqlDataAdapter(SQL_SCHEMA_GET, connection))
                        {
                            adapter.Fill(table);
                        }
                    }
                    return (table.Validated(), connectionString);
                }
                throw new ApplicationException($"Connection string for tenant {tenant} not found");
            }

            #endregion
        }

    }
    public interface IDatabaseService
    {
        Task ProcessEvent(EventData evt);
    }
}
