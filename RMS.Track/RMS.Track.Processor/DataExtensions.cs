﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace RMS.Track.Processor
{
    static class DataExtensions
    {
        internal static DataTable Validated(this DataTable table)
        {
            bool valid = new (string name, Type type)[]
            {
                ("EmailMessageID", typeof(long))
                ,("EventType", typeof(string))
                ,("Email", typeof(string))
                ,("EventTS", typeof(DateTime))
                ,("Url", typeof(string))
                ,("CreatedID", typeof(int))
                ,("CreatedSessionID", typeof(long))
                ,("CreatedTS", typeof(DateTime))
            }
            .All(colDef => table.Columns.Contains(colDef.name) && table.Columns[colDef.name].DataType.Equals(colDef.type));
            if (!valid) throw new ApplicationException("Data layer unhealthy");
            return table;
        }

        internal static void FillWith(this DataRow row, SendGridEvent sendGridEvent)
        {
            try
            {
                row["EmailMessageID"] = sendGridEvent.EmailId;
                row["EventType"] = sendGridEvent.EventType;
                row["Email"] = sendGridEvent.Email;
                row["EventTS"] = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(sendGridEvent.TimeStamp);
                row["Url"] = sendGridEvent.Url;
                row["CreatedID"] = -2;
                row["CreatedSessionID"] = -2;
                row["CreatedTS"] = DateTime.UtcNow;
                //row["ModifiedID"]
                //row["ModifiedSessionID"]
                //row["ModifiedTS"]
                //row["InactiveID"]
                //row["InactiveSessionID"]
                //row["InactiveTS"]
                row["StatusID"] = 1;

            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Error copying {sendGridEvent} into table row", ex);
            }
        }
    }
}

/*
    [EmailEventID] [bigint] IDENTITY(1,1) NOT NULL,
    [EmailMessageID] [bigint] NOT NULL,
    [EventType] [nvarchar](50) NOT NULL,
    [Email] [nvarchar](200) NULL,
    [EventTS] [datetime2](7) NOT NULL,
    [Url] [nvarchar](2050) NULL,
    [CreatedID] [int] NOT NULL,
    [CreatedSessionID] [bigint] NOT NULL,
    [CreatedTS] [datetime2](7) NOT NULL,
    [ModifiedID] [int] NULL,
    [ModifiedSessionID] [bigint] NULL,
    [ModifiedTS] [datetime2](7) NULL,
    [InactiveID] [int] NULL,
    [InactiveSessionID] [bigint] NULL,
    [InactiveTS] [datetime2](7) NULL,
    [StatusID] [int] NOT NULL,
*/