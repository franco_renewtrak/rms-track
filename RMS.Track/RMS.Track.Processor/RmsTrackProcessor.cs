﻿using Microsoft.Azure.EventHubs;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("RMS.Track.Tests")]

namespace RMS.Track.Processor
{
    public class RmsTrackProcessor
    {
        readonly IDatabaseService _db;

        public RmsTrackProcessor(IDatabaseService db) => _db = db;

        [FunctionName("RmsTrackProcessor")]
        public async Task Run([EventHubTrigger("sendgridwebhook", Connection = "EventHubConnectionString")] EventData[] events, ILogger log)
        {
            var exceptions = new List<Exception>();

            foreach (var evt in events)
            {
                try
                {
                   await _db.ProcessEvent(evt);
                }
                catch(Exception ex)
                {
                    exceptions.Add(ex);
                }
            }

            if (exceptions.Count > 1) throw new AggregateException(exceptions);

            if (exceptions.Count == 1) throw exceptions.Single();
        }
    }
}