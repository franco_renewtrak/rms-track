param namespaces_emailactivity_name string = 'emailactivity'
param serverfarms_AustraliaSoutheastPlan_name string = 'AustraliaSoutheastPlan'
param sites_EmailActivityWebHookReceiver_name string = 'EmailActivityWebHookReceiver'
param storageAccounts_emailactivitystorage_name string = 'emailactivitystorage'

resource namespaces_emailactivity_name_resource 'Microsoft.EventHub/namespaces@2021-01-01-preview' = {
  name: namespaces_emailactivity_name
  location: 'Australia Southeast'
  sku: {
    name: 'Basic'
    tier: 'Basic'
    capacity: 1
  }
  properties: {
    zoneRedundant: false
    isAutoInflateEnabled: false
    maximumThroughputUnits: 0
    kafkaEnabled: false
  }
}

resource storageAccounts_emailactivitystorage_name_resource 'Microsoft.Storage/storageAccounts@2021-04-01' = {
  name: storageAccounts_emailactivitystorage_name
  location: 'australiasoutheast'
  tags: {
    'hidden-related:/providers/Microsoft.Web/sites/EmailActivityWebHookReceiver': 'empty'
  }
  sku: {
    name: 'Standard_LRS'
    tier: 'Standard'
  }
  kind: 'Storage'
  properties: {
    networkAcls: {
      bypass: 'AzureServices'
      virtualNetworkRules: []
      ipRules: []
      defaultAction: 'Allow'
    }
    supportsHttpsTrafficOnly: true
    encryption: {
      services: {
        file: {
          keyType: 'Account'
          enabled: true
        }
        blob: {
          keyType: 'Account'
          enabled: true
        }
      }
      keySource: 'Microsoft.Storage'
    }
  }
}

resource serverfarms_AustraliaSoutheastPlan_name_resource 'Microsoft.Web/serverfarms@2018-02-01' = {
  name: serverfarms_AustraliaSoutheastPlan_name
  location: 'Australia Southeast'
  sku: {
    name: 'Y1'
    tier: 'Dynamic'
    size: 'Y1'
    family: 'Y'
    capacity: 0
  }
  kind: 'functionapp'
  properties: {
    perSiteScaling: false
    maximumElasticWorkerCount: 1
    isSpot: false
    reserved: false
    isXenon: false
    hyperV: false
    targetWorkerCount: 0
    targetWorkerSizeId: 0
  }
}

resource namespaces_emailactivity_name_RootManageSharedAccessKey 'Microsoft.EventHub/namespaces/AuthorizationRules@2021-01-01-preview' = {
  parent: namespaces_emailactivity_name_resource
  name: 'RootManageSharedAccessKey'
  location: 'Australia Southeast'
  properties: {
    rights: [
      'Listen'
      'Manage'
      'Send'
    ]
  }
}

resource namespaces_emailactivity_name_sendgridwebhook 'Microsoft.EventHub/namespaces/eventhubs@2021-01-01-preview' = {
  parent: namespaces_emailactivity_name_resource
  name: 'sendgridwebhook'
  location: 'Australia Southeast'
  properties: {
    messageRetentionInDays: 1
    partitionCount: 2
    status: 'Active'
  }
}

resource namespaces_emailactivity_name_default 'Microsoft.EventHub/namespaces/networkRuleSets@2021-01-01-preview' = {
  parent: namespaces_emailactivity_name_resource
  name: 'default'
  location: 'Australia Southeast'
  properties: {
    defaultAction: 'Deny'
    virtualNetworkRules: []
    ipRules: []
  }
}

resource storageAccounts_emailactivitystorage_name_default 'Microsoft.Storage/storageAccounts/blobServices@2021-04-01' = {
  parent: storageAccounts_emailactivitystorage_name_resource
  name: 'default'
  sku: {
    name: 'Standard_LRS'
    tier: 'Standard'
  }
  properties: {
    cors: {
      corsRules: []
    }
    deleteRetentionPolicy: {
      enabled: false
    }
  }
}

resource Microsoft_Storage_storageAccounts_fileServices_storageAccounts_emailactivitystorage_name_default 'Microsoft.Storage/storageAccounts/fileServices@2021-04-01' = {
  parent: storageAccounts_emailactivitystorage_name_resource
  name: 'default'
  sku: {
    name: 'Standard_LRS'
    tier: 'Standard'
  }
  properties: {
    protocolSettings: {
      smb: {}
    }
    cors: {
      corsRules: []
    }
    shareDeleteRetentionPolicy: {
      enabled: true
      days: 7
    }
  }
}

resource Microsoft_Storage_storageAccounts_queueServices_storageAccounts_emailactivitystorage_name_default 'Microsoft.Storage/storageAccounts/queueServices@2021-04-01' = {
  parent: storageAccounts_emailactivitystorage_name_resource
  name: 'default'
  properties: {
    cors: {
      corsRules: []
    }
  }
}

resource Microsoft_Storage_storageAccounts_tableServices_storageAccounts_emailactivitystorage_name_default 'Microsoft.Storage/storageAccounts/tableServices@2021-04-01' = {
  parent: storageAccounts_emailactivitystorage_name_resource
  name: 'default'
  properties: {
    cors: {
      corsRules: []
    }
  }
}

resource sites_EmailActivityWebHookReceiver_name_resource 'Microsoft.Web/sites@2018-11-01' = {
  name: sites_EmailActivityWebHookReceiver_name
  location: 'Australia Southeast'
  kind: 'functionapp'
  identity: {
    principalId: '03c01824-84dd-447d-b2e9-c35c13220a2b'
    tenantId: '5fe5a52d-9848-4817-9fe5-4d9fff7386f3'
    type: 'SystemAssigned'
  }
  properties: {
    enabled: true
    hostNameSslStates: [
      {
        name: 'emailactivitywebhookreceiver.azurewebsites.net'
        sslState: 'Disabled'
        hostType: 'Standard'
      }
      {
        name: 'emailactivitywebhookreceiver.scm.azurewebsites.net'
        sslState: 'Disabled'
        hostType: 'Repository'
      }
    ]
    serverFarmId: serverfarms_AustraliaSoutheastPlan_name_resource.id
    reserved: false
    isXenon: false
    hyperV: false
    siteConfig: {
      numberOfWorkers: 1
      alwaysOn: false
      http20Enabled: false
    }
    scmSiteAlsoStopped: false
    clientAffinityEnabled: false
    clientCertEnabled: false
    hostNamesDisabled: false
    containerSize: 1536
    dailyMemoryTimeQuota: 0
    httpsOnly: true
    redundancyMode: 'None'
  }
}

resource sites_EmailActivityWebHookReceiver_name_web 'Microsoft.Web/sites/config@2018-11-01' = {
  parent: sites_EmailActivityWebHookReceiver_name_resource
  name: 'web'
  location: 'Australia Southeast'
  properties: {
    numberOfWorkers: 1
    defaultDocuments: [
      'Default.htm'
      'Default.html'
      'Default.asp'
      'index.htm'
      'index.html'
      'iisstart.htm'
      'default.aspx'
      'index.php'
    ]
    netFrameworkVersion: 'v4.0'
    phpVersion: '5.6'
    requestTracingEnabled: false
    remoteDebuggingEnabled: true
    remoteDebuggingVersion: 'VS2019'
    httpLoggingEnabled: false
    logsDirectorySizeLimit: 35
    detailedErrorLoggingEnabled: false
    publishingUsername: '$EmailActivityWebHookReceiver'
    azureStorageAccounts: {}
    scmType: 'None'
    use32BitWorkerProcess: true
    webSocketsEnabled: false
    alwaysOn: false
    managedPipelineMode: 'Integrated'
    virtualApplications: [
      {
        virtualPath: '/'
        physicalPath: 'site\\wwwroot'
        preloadEnabled: false
      }
    ]
    loadBalancing: 'LeastRequests'
    experiments: {
      rampUpRules: []
    }
    autoHealEnabled: false
    localMySqlEnabled: false
    managedServiceIdentityId: 4745
    ipSecurityRestrictions: [
      {
        ipAddress: 'Any'
        action: 'Allow'
        priority: 1
        name: 'Allow all'
        description: 'Allow all access'
      }
    ]
    scmIpSecurityRestrictions: [
      {
        ipAddress: 'Any'
        action: 'Allow'
        priority: 1
        name: 'Allow all'
        description: 'Allow all access'
      }
    ]
    scmIpSecurityRestrictionsUseMain: false
    http20Enabled: false
    minTlsVersion: '1.2'
    ftpsState: 'AllAllowed'
    reservedInstanceCount: 0
  }
}

resource sites_EmailActivityWebHookReceiver_name_0a0247b9559b44f5a260c8be516037ec 'Microsoft.Web/sites/deployments@2018-11-01' = {
  parent: sites_EmailActivityWebHookReceiver_name_resource
  name: '0a0247b9559b44f5a260c8be516037ec'
  location: 'Australia Southeast'
  properties: {
    status: 4
    author_email: 'N/A'
    author: 'N/A'
    deployer: 'ZipDeploy'
    message: 'Created via a push deployment'
    start_time: '15/06/2021 10:02:57 AM'
    end_time: '15/06/2021 10:03:03 AM'
    active: true
  }
}

resource sites_EmailActivityWebHookReceiver_name_221c859abd7a45c19fe70c5c08dbe7ce 'Microsoft.Web/sites/deployments@2018-11-01' = {
  parent: sites_EmailActivityWebHookReceiver_name_resource
  name: '221c859abd7a45c19fe70c5c08dbe7ce'
  location: 'Australia Southeast'
  properties: {
    status: 4
    author_email: 'N/A'
    author: 'N/A'
    deployer: 'ZipDeploy'
    message: 'Created via a push deployment'
    start_time: '15/06/2021 6:24:17 AM'
    end_time: '15/06/2021 6:24:26 AM'
    active: false
  }
}

resource sites_EmailActivityWebHookReceiver_name_d76036735b92421fb7f069eb3594a8f7 'Microsoft.Web/sites/deployments@2018-11-01' = {
  parent: sites_EmailActivityWebHookReceiver_name_resource
  name: 'd76036735b92421fb7f069eb3594a8f7'
  location: 'Australia Southeast'
  properties: {
    status: 4
    author_email: 'N/A'
    author: 'N/A'
    deployer: 'ZipDeploy'
    message: 'Created via a push deployment'
    start_time: '15/06/2021 6:49:44 AM'
    end_time: '15/06/2021 6:49:49 AM'
    active: false
  }
}

resource sites_EmailActivityWebHookReceiver_name_Function1 'Microsoft.Web/sites/functions@2018-11-01' = {
  parent: sites_EmailActivityWebHookReceiver_name_resource
  name: 'Function1'
  location: 'Australia Southeast'
  properties: {
    script_root_path_href: 'https://emailactivitywebhookreceiver.azurewebsites.net/admin/vfs/site/wwwroot/Function1/'
    script_href: 'https://emailactivitywebhookreceiver.azurewebsites.net/admin/vfs/site/wwwroot/bin/FunctionApp1.dll'
    config_href: 'https://emailactivitywebhookreceiver.azurewebsites.net/admin/vfs/site/wwwroot/Function1/function.json'
    href: 'https://emailactivitywebhookreceiver.azurewebsites.net/admin/functions/Function1'
    config: {}
  }
}

resource sites_EmailActivityWebHookReceiver_name_sites_EmailActivityWebHookReceiver_name_azurewebsites_net 'Microsoft.Web/sites/hostNameBindings@2018-11-01' = {
  parent: sites_EmailActivityWebHookReceiver_name_resource
  name: '${sites_EmailActivityWebHookReceiver_name}.azurewebsites.net'
  location: 'Australia Southeast'
  properties: {
    siteName: 'EmailActivityWebHookReceiver'
    hostNameType: 'Verified'
  }
}

resource namespaces_emailactivity_name_sendgridwebhook_Default 'Microsoft.EventHub/namespaces/eventhubs/consumergroups@2021-01-01-preview' = {
  parent: namespaces_emailactivity_name_sendgridwebhook
  name: '$Default'
  location: 'Australia Southeast'
  properties: {}
  dependsOn: [
    namespaces_emailactivity_name_resource
  ]
}

resource storageAccounts_emailactivitystorage_name_default_azure_webjobs_hosts 'Microsoft.Storage/storageAccounts/blobServices/containers@2021-04-01' = {
  parent: storageAccounts_emailactivitystorage_name_default
  name: 'azure-webjobs-hosts'
  properties: {
    defaultEncryptionScope: '$account-encryption-key'
    denyEncryptionScopeOverride: false
    publicAccess: 'None'
  }
  dependsOn: [
    storageAccounts_emailactivitystorage_name_resource
  ]
}

resource storageAccounts_emailactivitystorage_name_default_azure_webjobs_secrets 'Microsoft.Storage/storageAccounts/blobServices/containers@2021-04-01' = {
  parent: storageAccounts_emailactivitystorage_name_default
  name: 'azure-webjobs-secrets'
  properties: {
    defaultEncryptionScope: '$account-encryption-key'
    denyEncryptionScopeOverride: false
    publicAccess: 'None'
  }
  dependsOn: [
    storageAccounts_emailactivitystorage_name_resource
  ]
}

resource storageAccounts_emailactivitystorage_name_default_emailactivitywebhookreceiver 'Microsoft.Storage/storageAccounts/fileServices/shares@2021-04-01' = {
  parent: Microsoft_Storage_storageAccounts_fileServices_storageAccounts_emailactivitystorage_name_default
  name: 'emailactivitywebhookreceiver'
  properties: {
    accessTier: 'TransactionOptimized'
    shareQuota: 5120
    enabledProtocols: 'SMB'
  }
  dependsOn: [
    storageAccounts_emailactivitystorage_name_resource
  ]
}

resource storageAccounts_emailactivitystorage_name_default_AzureWebJobsHostLogs202106 'Microsoft.Storage/storageAccounts/tableServices/tables@2021-04-01' = {
  parent: Microsoft_Storage_storageAccounts_tableServices_storageAccounts_emailactivitystorage_name_default
  name: 'AzureWebJobsHostLogs202106'
  dependsOn: [
    storageAccounts_emailactivitystorage_name_resource
  ]
}

resource storageAccounts_emailactivitystorage_name_default_AzureWebJobsHostLogscommon 'Microsoft.Storage/storageAccounts/tableServices/tables@2021-04-01' = {
  parent: Microsoft_Storage_storageAccounts_tableServices_storageAccounts_emailactivitystorage_name_default
  name: 'AzureWebJobsHostLogscommon'
  dependsOn: [
    storageAccounts_emailactivitystorage_name_resource
  ]
}