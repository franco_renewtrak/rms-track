# rms-track
Project scope is collecting Twilio SendGrid Webhooks deliveries into a Renewtrak Management System database


## .NET Projects
* Common [RMS.Track.Common.csproj]
* Receiver [RMS.Track.Receiver.csproj]
* Processor [RMS.Track.Processor.csproj]
* Tests [RMS.Track.Tests.fsproj]

### Common [RMS.Track.Common.csproj]
.NET Class library with Key Vault access logic.

### Receiver [RMS.Track.Receiver.csproj]
Azure function app with one function:

* Name: __RmsTrackReceiver__ 
* Http trigger route: <App Url>/__analytics/email/{tenant}__ (to be configured in the SendGrid portal)
* Event Hub output binding: __emailactivity/sendgridwebhook__

#### Workflow
* Receive Web hook calls from Send Grid
* Validate Send Grid Web hook signature 
* Store authorized messages in an Azure event hub

### Processor [RMS.Track.Receiver.csproj]
Azure function app with one function:

* Name: __RmsTrackProcessor__ 
* Event Hub trigger: __emailactivity/sendgridwebhook__

#### Workflow
* Pick items from the event hub fed by the Receiver
* Transform them into database records 
* Commit records to database

### Tests [RMS.Track.Tests.fsproj]
Unit tests of the most important code blocks

## Azure resources
|Resource Group  
|-Key Vault  
|-Event Hubs Namespace named _emailactivity_ (reference in configuration)  
|--Event Hub named _sendgridwebhook_ (reference in code)  
|-App Service Plan  
|--Receiver Function App  
|---Receiver Function  
|---Receiver Storage  
|---Receiver Insights (optional)  
|--Processor Function App  
|---Processor Function  
|---Processor Storage  
|---Processor Insights (optional)

#### Key Vault
Contains the following secrets (per tenant):

* SendGrid web hook signature public key, named _{tenant}-sendgrid-webhook-publickey_
* Database connection string, named _{tenant}-connection-string_

The following (vault) Access policies must be defined:

* Get and List permissions for Secrets to Receiver Function App
* Get and List permissions for Secrets to Processor Function App

## Deployment
Visual Studio Publish profiles created for the two Function App projects.  
IaC creation to be assessed for cloud provisioning automation.

## Notes
Integration tests have been performed manually with Premium plan for the Function App (at least one warm ready instance) and EP1 pricing tier

Two separate Function Apps have been created instead of one App with two functions because scaling configuration is per App and not per function.
Presumably Processor execution should be slower than Receiver's, hence it is expected that Processor will need greater scaling up and out dotation.
Moreover, Receiver would need to scale on Send Grid web hook traffic, while Receiver on Event Hub load, hence the two apps should show
different scaling behaviour both in size than in time, hence the opportunity to have two different apps.

The suite is designed to allow multi tenancy, as follows:

* Web hook endpoints url have the format _analytics/email/{tenant}_ (one webhook url per tenant)
* Web hook signature public keys are per tenant. Receiver searches the Key Vault for _{tenant}-sendgrid-webhook-publickey_ secret
* Receiver embeds the tenant along the Send Grid json formatted events collection into the message stored in event hub
* Processor commits event records in the table named _EmailEvent_ (fixed), but the database connection string is retrieved by the Key Vault _{tenant}-connection-string_ secret

As such, the design allows the following scenario:

* Different tenants have a private webhook endpoint url
* Every tenant have their own private signature public key
* Events associated to a specific tenant are stored in a tenant-associated database

## Add new tenant/account
1

## Release History
### 22 June 2021 - First release
Tested by sending 2000 (fake) emails. All the 2000 associated events were stored in the database in about 20s.